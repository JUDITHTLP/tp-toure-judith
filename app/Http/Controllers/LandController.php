<?php

namespace App\Http\Controllers;

use App\Models\land;
use Illuminate\Http\Request;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('index',["lands" =>land::all()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle"=>"required|unique:lands,libelle",
            "description"=>"required",
            "continent"=>"required|in:EUROPE,AFRIQUE,ASIE,OCEANIE,ANTARCTIQUE,AMERIQUE",
            "population"=>"required",
            "capitale"=>"required",
            "superficie"=>"required",
            "code_indicatif"=>"required",
            "monnaie"=>"required",
            "langue"=>"required",
            "est_laique"=>"required",
        ]);
        land::create($request->all());
        return redirect()->route('index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function show(land $land)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lands = land::findOrFail($id);
        return view("modifier",["lands"=>$lands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $request->validate([
            "libelle"=>"required",
            "description"=>"required",
            "continent"=>"required",
            "population"=>"required",
            "capitale"=>"required",
            "superficie"=>"required",
            "code_indicatif"=>"required",
            "monnaie"=>"required",
            "langue"=>"required",
            "est_laique"=>"required",
        ]);

        $land = Land::find($id);
        $land->libelle = $request->libelle;
        $land->description = $request->description;
        $land->continent = $request->continent;
        $land->population = $request->population;
        $land->capitale = $request->capitale;
        $land->superficie = $request->superficie;
        $land->code_indicatif = $request->code_indicatif;
        $land->monnaie = $request->monnaie;
        $land->langue = $request->langue;
        $land->est_laique = $request->est_laique;
$land->update();
return redirect()->route('index');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        land::find($id)->delete();
        return redirect()->route("index");
    }
}
