<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table) {
            $table->id();

            $table->string("libelle")->nullable();

            $table->text("description");

            $table->enum('continent', ['AFRIQUE', 'EUROPE', 'AMERIQUE','ASIE','ANTARCTIQUE','OCEANIE']);

            $table->integer("population");

            $table->string("capitale");

            $table->integer("superficie");

            $table->text("code_indicatif")->unique();

            $table->enum('monnaie', ['XOF', 'EUR', 'DOLLAR']);

            $table->enum('langue', ['FR', 'EN', 'AR','ES']);

            $table->boolean("est_laique")->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
};
