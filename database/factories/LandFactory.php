<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [

            'libelle' =>$this->faker->country(),

            'description'=>$this->faker->paragraph(),

            'population'=>rand(1000,1000000000),

            'capitale'=>$this->faker->city(),

            'code_indicatif'=> $this->faker->bothify('+###'),

            'est_laique'=>$this->faker-> boolean(),
            'continent'=>$this->faker->randomElement(['Afrique','Asie','Amerique','Europe']),
            'monnaie'=>$this->faker->randomElement(['XOF', 'EUR', 'DOLLAR']),
            'langue'=>$this->faker->randomElement(['FR', 'EN', 'AR','ES']),

            'superficie'=>rand(60000,1750000),
        ];
    }
}
