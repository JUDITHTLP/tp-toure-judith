<?php
use App\Http\Controllers\LandController;
use App\Models\Land;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.main');
});

Route::get('/h', function () {
    $lands = Land::all();

    return view('land', ["lands" => $lands])

   ;
});

Route::get("/",[LandController::class, 'index'])->name('index');
Route::get("/ajouter",[LandController::class, 'create'])->name('creer');

Route::post("/hhh",[LandController::class, 'store'])->name('stor');

Route::get("/modif/{id}",[LandController::class, 'edit'])->name('modifier');
Route::post("/MAJ/{id}",[LandController::class, 'update'])->name('miseAJ');

Route::get("/sup/{id}",[LandController::class, 'destroy'])->name('supprimer');

