@extends('layouts.main')

@section('content')
    <div class="card-body">
        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
            <div class="row">
                <div class="col-sm-12 col-md-6"></div><a href="{{route('creer')}}">
                    <button class="btn btn-primary btn-circle" type="button"> 
                        ajouter un pays
                    </button>
                    </a>
                <div class="col-sm-12 col-md-6"></div>
            </div>
            <div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div><h2>LANDS</h2></div>
                    <table id="example2" class="table-bordered table-hover dataTable dtr-inline" role="grid"
                        aria-describedby="example2_info">
                        
                        <thead>
                            <tr role="row">
                                <th scope="col">ID</th>
                                <th scope="col">Libelle</th>
                                <th scope="col">Description</th>
                                <th scope="col">Continent</th>
                                <th scope="col">Population</th>
                                <th scope="col">Capitale</th>
                                <th scope="col">Superficie</th>
                                <th scope="col">code_Indicatif</th>
                                <th scope="col">Monnaie</th>
                                <th scope="col">Langue</th>
                                <th scope="col">Est_laique</th>
                                <th scope="col">Actions</th>


                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach ($lands as $land)
                                <tr class="odd">
                                    <td class="dtr-control sorting_1" tabindex="0">{{ $land->id }}</td>
                                    <td>{{ $land->libelle }}</td>
                                    <td>{{ $land->description }}</td>
                                    <td>{{ $land->continent }}</td>
                                    <td>{{ $land->population }}</td>
                                    <td>{{ $land->capitale }}</td>
                                    <td>{{ $land->superficie }}</td>
                                    <td>{{ $land->code_indicatif }}</td>
                                    <td>{{ $land->monnaie }}</td>
                                    <td>{{ $land->langue }}</td>
                                    <td>{{ $land->est_laique ? 'Oui' : 'Non' }}</td>
                                    <td class="text-nowrap">
                                        
                                            
                                    <a href="{{route('modifier', ["id"=>$land->id])}}">
                                    <button class="btn btn-primary btn-circle" type="button"> 
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    </a>
                                    <a href="{{route('supprimer', ["id"=>$land->id])}}">
                                        <button class="btn btn-primary btn-circle" type="button"> 
                                            <i class="fas fa-trash"></i>
                                        </button>
    
                                        </a>
                                    
                                    </td>

                                </tr>
                            @endforeach


                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    @endsection
